// @todo: build css filename with hash
// @todo: sprites // https://www.npmjs.com/package/webpack-spritesmith

// <DoNotDelete>
// Useful links to learn config
// https://github.com/zeit/next-plugins/issues/157
// https://github.com/zeit/next-plugins/issues/127
// </DoNotDelete>

// check examples:
// https://github.com/orbiting/construction
// https://github.com/opencollective/frontend
// https://github.com/nteract/nteract.io
// https://github.com/PlatziDev/pulse
// https://github.com/wanoyume/webiste-jp
// https://github.com/builderbook/builderbook

const withSass = require('@zeit/next-sass');
const withPlugins = require('next-compose-plugins');

const dev = (process.env.NODE_ENV !== 'production');

const withSassConfig = {
  poweredByHeader: false,
  distDir: 'build',
  cssLoaderOptions: {
    sourceMap: dev,
  },
  sassLoaderOptions: {
    sourceMap: dev,
  },
};

const postcssSourceMapFix = {
  webpack(config, options) {
    if (dev && config.name === 'client') {
      // eslint-disable-next-line
      config.module.rules = config.module.rules.map((item) => {
        if (typeof item.use.loader === 'undefined') {
          // eslint-disable-next-line
          item.use = item.use.map((loader) => {
            if (typeof loader === 'object' && loader.loader === 'postcss-loader' && typeof loader.options === 'object') {
              // eslint-disable-next-line
              loader.options.sourceMap = true;
            }

            return loader;
          });
        }

        return item;
      });
    }

    return config;
  },
};

module.exports = withPlugins([
  [withSass, withSassConfig],
], postcssSourceMapFix);
