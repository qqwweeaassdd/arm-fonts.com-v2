const Config = require('config');

let db = null;

/**
 * Init database
 * @returns {*}
 */
const init = () => (
  Config.require('models')()
    .then((database) => {
      db = database;
      return database;
    })
);

/**
 * Get database or model if modelName is specified
 * @param modelName string
 * @returns {*}
 */
const get = (modelName) => {
  if (!db) {
    throw new Error('DB is not initialized! Use Config.initDB() first.');
  }

  return (typeof modelName === 'undefined' ? db : db[modelName]);
};

/**
 * Get all items of all models
 * @returns {object}
 */
const getAll = () => Promise.all([
  get('FontName').getAll(),
  get('Font').getAll(),
]);

// const plainify = (items) => items.map((item) => item.get());

module.exports = {
  init,
  get,
  getAll,
};
