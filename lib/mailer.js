const Config = require('config');
const nodemailer = require('nodemailer');

const mailer = Config.get('mailer');
const toEmail = mailer.auth.user;
const transporter = nodemailer.createTransport(mailer);

module.exports = (data) => {
  const params = {
    to: toEmail,
    from: `${data.name} <${toEmail}>`,
    subject: 'Armenian Fonts message',
    text: `Name: ${data.name},
Email: ${data.email},
Message:
${data.message}`,
  };

  return new Promise((resolve) => {
    transporter.sendMail(params, (error, res) => {
      if (error) {
        console.error(error);
      } else {
        console.log(res);
      }

      return resolve(error);
    });
  });
};
