const IsomorphicUnfetch = require('isomorphic-unfetch');

/*
Example:

async func() {
  // your code

  try {
    const res = await fetch('/api/url', data);

    if (res.status) {
      // your code
    } else {
      // data error
      console.error(res);
    }
  } catch (err) {
    // server error
    console.error(err);
  }
}
*/

const defaultParams = {
  method: 'post',
  headers: {
    Accept: 'application/json, text/plain, */*',
    'Content-Type': 'application/json',
  },
  body: '',
};

function checkStatus(response) {
  if (response.ok) {
    return response;
  }

  const error = new Error(`${response.status}: ${response.statusText} (${response.url})`);
  error.response = response;
  throw error;
}

const fetch = async (url, sendData, params) => {
  const sendParams = {
    ...defaultParams,
    ...params || {},
  };

  // 2-nd level params
  if (params) {
    Object.keys(defaultParams).forEach((key) => {
      if (typeof defaultParams[key] === 'object' && typeof params[key] !== 'undefined') {
        sendParams[key] = {
          ...defaultParams[key],
          ...params[key],
        };
      }
    });
  }

  if (sendData) {
    sendParams.body = JSON.stringify(sendData);
  }

  const res = await IsomorphicUnfetch(url, sendParams);
  const data = checkStatus(res);
  return data.json();
};

module.exports = {
  fetch,
};
