const Fs = require('fs');
const Path = require('path');
const Sequelize = require('sequelize');
const Config = require('config');

const storagePath = Path.join(`${Config.get('storage')}`, 'db.sqlite');
const basename = Path.basename(module.filename);

let database;

function initDB() {
  if (typeof database !== 'undefined') {
    return database;
  }

  return new Promise((resolve) => {
    const db = {};
    const dbConfig = {
      dialect: 'sqlite',
      storage: storagePath,
      logging: Config.get('db.logging'),
      operatorsAliases: false,
      define: {
        timestamps: false,
        freezeTableName: true,
      },
    };

    const sequelize = new Sequelize(dbConfig);

    // get models
    Fs.readdirSync(__dirname)
      .filter((file) => (file.indexOf('.') !== 0) && (file !== basename) && file.slice(-3) === '.js') // get model files
      .map((file) => sequelize.import(Path.join(__dirname, file))) // get models
      .forEach((model) => { db[model.name] = model; }); // save model

    // build associations
    Object.keys(db)
      .filter((modelName) => (typeof db[modelName].associate === 'function'))
      .forEach((modelName) => db[modelName].associate(db));

    db.sequelize = sequelize;
    db.Sequelize = Sequelize;

    database = db;

    // get DB items
    db.sequelize.sync()
      .then(() => Promise.all([db.Font.getAll(), db.FontName.getAll()]))
      .then(() => resolve(database));
  });
}

module.exports = initDB;
