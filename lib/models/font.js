module.exports = function (sequelize, DataTypes) {
  // all items will be cached
  let items = null;

  const Font = sequelize.define('Font', {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    fontNameId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      references: {
        model: 'FontName',
        key: 'id',
      },
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sourceFilename: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    weight: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: 400,
    },
    italic: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0,
    },
    glyphType: {
      type: DataTypes.INTEGER(3),
      allowNull: false,
      defaultValue: 0,
    },
    downloadsCnt: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
    },
    fontSubfamily: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    fullName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    version: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    uniqueID: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    postScriptName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    copyright: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    trademark: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    manufacturer: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    designer: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    fontVendorURL: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    fontDesignerURL: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    licenseDescription: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    licenseURL: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    preferredFamily: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    preferredSubfamily: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    compatibleFullName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    sampleText: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    VendorID: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    indexes: [],
  });

  Font.associate = (models) => Font.belongsTo(models.FontName);

  /**
   * Get all items of this model. Caching
   * @returns {array}
   */
  Font.getAll = function getAll() {
    return items || this.findAll()
      .then((res) => {
        items = res;
        return res;
      });
  };

  return Font;
};
