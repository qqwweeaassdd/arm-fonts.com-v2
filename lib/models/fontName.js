module.exports = function (sequelize, DataTypes) {
  // all items will be cached
  let items = null;

  const FontName = sequelize.define('FontName', {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    viewsCnt: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
    },
    IsMonospaced: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0,
    },
    IsSerif: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0,
    },
  });

  FontName.associate = (models) => FontName.hasMany(models.Font);

  /**
   * Get all items of this model. Caching
   * @returns {array}
   */
  FontName.getAll = function getAll() {
    return items || this.findAll()
      .then((res) => {
        items = res;
        return res;
      });
  };

  /**
   * Get all items of all models
   * @returns {object}
   */
  /* FontName.getDB = function getAll() {
    console.log(sequelize.models.Font);
    return Promise.all([this.getAll(), sequelize.models.Font.getAll()]);
  }; */

  return FontName;
};
