const Express = require('express');

const router = Express.Router({});

router.get('/download/:fontname', (req, res, next) => {
  const actualPage = '/download';
  const queryParams = {
    fontname: req.params.fontname,
  };

  const app = req.app.get('next');
  app.render(req, res, actualPage, queryParams);
});

module.exports = router;
