const Express = require('express');
const Next = require('next');
const Url = require('url');
const Config = require('config');
const CookieParser = require('cookie-parser');
const BodyParser = require('body-parser');
const compression = require('compression');

const db = Config.require('db');

// require express routes
const routesAPI = require('./routes-api');
const routes = require('./routes');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = (process.env.NODE_ENV !== 'production');

// init next.js
const app = Next({ dev });
const handle = app.getRequestHandler();

const promises = [
  app.prepare(), // start next.js
  db.init(), // init sequelize DB
];

Promise.all(promises)
  .then(() => {
    // express
    const server = Express();
    server.set('next', app);

    // express: server compression
    if (!dev) {
      server.use(compression());
    }

    server.disable('x-powered-by');

    // express parser middlewares
    server.use(BodyParser.json());
    server.use(BodyParser.urlencoded({ extended: false }));
    server.use(CookieParser());

    // express static
    server.use(Express.static(Config.get('staticPath')));

    // express trailing slash middleware
    server.use((req, res, next) => {
      const url = Url.parse(req.url);

      if (url.pathname.substr(-1) === '/' && url.pathname.length > 1) {
        url.pathname = url.pathname.slice(0, -1);
        res.redirect(301, url.format(url));
      } else {
        next();
      }
    });

    // express routes
    server.use('/api', routesAPI);
    server.use('/', routes);

    // route for express: everything else have to be handled by next.js
    server.get('*', (req, res) => (
      handle(req, res)
    ));

    // start express
    server.listen(port, (err) => {
      if (err) {
        throw err;
      }

      // eslint-disable-next-line
      console.log(`> Ready on http://localhost:${port}`);
    });
  }).catch((err) => {
    // eslint-disable-next-line
    console.error(err.stack);
    process.exit(1);
  });
