const Express = require('express');
const Config = require('config');

const db = Config.require('db');

// @todo: ajax loader

const { validateEmail, getFieldError } = Config.require('funcs');
const mailer = Config.require('mailer');

const router = Express.Router({});

router.post('/data', (req, res, next) => {
  const result = {
    status: true,
    errors: [],
    result: {},
  };

  db.getAll()
    .then((tables) => {
      result.data = {
        fontName: tables[0],
        font: tables[1],
      };

      res.json(result);
    });
});

router.post('/contacts', (req, res, next) => {
  const { name, email, message } = req.body;
  const result = {
    status: true,
    errors: [],
    result: {},
  };

  // validations
  let requiredError = null;
  if (!name) {
    requiredError = 'All fields are required.';
    result.errors.push(requiredError);
    result.errors.push(getFieldError('name', null));
  }

  if (!email) {
    if (!requiredError) {
      requiredError = 'All fields are required.';
      result.errors.push(requiredError);
    }
    result.errors.push(getFieldError('email', null));
  } else if (!validateEmail(email)) {
    result.errors.push(getFieldError('email', 'Email is not valid.'));
  }

  if (!message) {
    if (!requiredError) {
      requiredError = 'All fields are required.';
      result.errors.push(requiredError);
    }
    result.errors.push(getFieldError('message', null));
  }

  // send mail
  mailer({ name, email, message })
    .then((error) => {
      if (error) {
        result.errors.push('Server error: can not send message. Try again later.');
      }

      result.status = (result.errors.length === 0);

      return res.json(result);
    });
});

module.exports = router;
