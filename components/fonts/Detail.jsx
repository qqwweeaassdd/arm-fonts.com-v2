import React from 'react';
import Link from 'next/link';

const Component = () => (
  <article>
    <div className="font-title">
      <h5>Arial AMU Regular</h5>
      <span className="version font-accent">&nbsp;(.ttf, variant 1)</span>
    </div>
    <div className="font-sample">
      <img
        data-original="http://arm-fonts.com/app/samples/Arial-AMU-Regular-8891.png"
        className="lazy"
        alt="Arial AMU Regular sample"
        width="700"
        height="160"
        src="http://arm-fonts.com/app/samples/Arial-AMU-Regular-8891.png"
        style={{ display: 'inline' }}
      />
    </div>

    <div className="params-line">
      <div className="params">
        <div className="params-title">Description:</div>
        <div className="param">
          <span className="font-accent">Font weight:</span>
          {' Regular'}
        </div>
        <div className="param">
          <span className="font-accent">Font style:</span>
          {' Normal'}
        </div>
        <div className="param">
          <span className="font-accent">Font format:</span>
          {' TrueType'}
        </div>
        <div className="param">
          <span className="font-accent">Original full name:</span>
          {' Arial AMU'}
        </div>
        <div className="param">
          <span className="font-accent">Manufacturer:</span>
          {' Ruben Tarumian'}
        </div>
      </div>

      <div className="params-link">
        <Link href="/font-detail">
          <a href="/font-detail" className="font-detail-link font-accent" title="Download Arial AMU font family">
            <i className="fas fa-download" />
            {' '}
            <span>Download font</span>
          </a>
        </Link>
      </div>
    </div>
  </article>
);

Component.displayName = 'FontsDetail';

export default Component;
