import React from 'react';
import Link from 'next/link';

const Component = () => (
  <article>
    <div className="font-title">
      <h5>
        <Link href="/font-detail">
          <a href="/font-detail" title="Arial AMU free Armenian font">Arial AMU Regular</a>
        </Link>
      </h5>
      <span className="version font-accent">&nbsp;(.ttf, variant 1)</span>
    </div>
    <div className="font-sample">
      <img
        data-original="http://arm-fonts.com/app/samples/Arial-AMU-Regular-8891.png"
        className="js-lazy"
        alt="Arial AMU Regular sample"
        src="http://arm-fonts.com/app/samples/Arial-AMU-Regular-8891.png"
      />
    </div>

    <div className="params-line">
      <div className="params">
        <div className="font-downloads font-accent">5709 downloads &nbsp;::&nbsp; 19607 views</div>
      </div>
      <div className="params-link">
        <Link href="/font-detail">
          <a href="/font-detail" className="font-detail-link font-accent" title="Download Arial AMU font family">
            <i className="fas fa-font" />
            {' '}
            <span>Font details</span>
          </a>
        </Link>
      </div>
    </div>
  </article>
);

Component.displayName = 'FontsPreview';

export default Component;
