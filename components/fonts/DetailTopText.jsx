// @flow

import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import ClassNames from 'classnames';

import withExpanded from 'components/HOC/withExpanding';

type Props = {
  isExpanded: boolean,
  onClickToggle: Function,
};

const propTypes = {
  isExpanded: PropTypes.bool.isRequired,
  onClickToggle: PropTypes.func.isRequired,
};

const Component = ({ isExpanded, onClickToggle }: Props) => (
  <div className={ClassNames('detail-page-info', { visible: isExpanded })}>
    <div className="back-link">
      <i className="fas fa-long-arrow-alt-left" />
      {' Back to all '}
      <Link href="/">
        <a href="/">Armenian fonts</a>
      </Link>
      {' list'}
    </div>

    <p>
      {'The fonts can be used as regular english fonts either.'}
    </p>

    <p>
      {'You can download and use this Armenian font for all modern version of Windows OS, Linux or Mac OS. '}
      <span className={ClassNames('link', { visible: isExpanded })} onClick={onClickToggle}>more...</span>
    </p>

    <div className="more-text">
      <p>
        {'This are preview pictures of Arial AMU Armenian font family items with Armenian letters, english letters and numbers. Arial AMU font family is a group of proportional sans serif fonts. Here are represented next font variants: Arial AMU Regular Normal TrueType, Arial AMU Regular Italic TrueType, Arial AMU Bold Normal TrueType, Arial AMU Bold Italic TrueType, Arian AMU, Arial Armenian.'}
      </p>
      <p>
        {'Arial AMU font family is a classical Arial font with support of Armenian letters. People often call it Arial Armenian font. This font is a very popular typeface which is used by millions of people all over the world.'}
      </p>
      <p>
        {'This page contains unicode Arial Armenian font versions and can be easily used on modern computers. Here you can find out similar to Arial fonts: '}
        <Link href="/font-detail">
          <a href="/font-detail">FreeSans Armenian font download</a>
        </Link>
        {' and '}
        <Link href="/font-detail">
          <a href="/font-detail">DejaVu Sans Armenian font download</a>
        </Link>
        {'.'}
      </p>
    </div>
  </div>
);

Component.propTypes = propTypes;
Component.displayName = 'DetailTopText';

export default withExpanded(Component);
