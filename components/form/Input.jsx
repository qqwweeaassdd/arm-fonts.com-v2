// @flow

import React, { type createRef } from 'react';
import PropTypes from 'prop-types';

import FormContacts from 'components/form/Contacts';

type Props = {
  type?: string,
  className?: string,
  name: string,
  id: string,
  placeholder: string,
  value: string,
  maxLength?: number,
};

const propTypes = {
  type: PropTypes.string,
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  maxLength: PropTypes.number,
};

const defaultProps = {
  type: 'text',
  className: '',
  maxLength: 100,
};

const Input = (props: Props, ref: createRef<FormContacts>) => (
  <input
    ref={ref}
    {...props}
  />
);

const Component = React.forwardRef(Input);

Component.propTypes = propTypes;
Component.defaultProps = defaultProps;
Component.displayName = 'Input';

export default Component;
