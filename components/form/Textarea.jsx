// @flow

import React, { type createRef } from 'react';
import PropTypes from 'prop-types';

import FormContacts from 'components/form/Contacts';

type Props = {
  className?: string,
  name: string,
  id: string,
  placeholder: string,
  value: string,
  maxLength?: number,
};

const propTypes = {
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  maxLength: PropTypes.number,
};

const defaultProps = {
  className: '',
  maxLength: 10000,
};

const Textarea = (props: Props, ref: createRef<FormContacts>) => (
  <textarea
    cols="1"
    rows="1"
    ref={ref}
    {...props}
  />
);

const Component = React.forwardRef(Textarea);

Component.propTypes = propTypes;
Component.defaultProps = defaultProps;
Component.displayName = 'Textarea';

export default Component;
