import React from 'react';
import PropTypes from 'prop-types';

import { fetch } from 'lib/fetch';
import { validateEmail } from 'lib/funcs';

const propTypes = {
  children: PropTypes.func.isRequired,
};

class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nameRef: React.createRef(),
      emailRef: React.createRef(),
      messageRef: React.createRef(),
      nameValue: '',
      emailValue: '',
      messageValue: '',
      nameIsError: false,
      emailIsError: false,
      messageIsError: false,
      errorMessage: [],
      successMessage: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  setFocus(field) {
    // eslint-disable-next-line
    if (field && typeof this.state[`${field}Ref`] !== 'undefined') {
      // eslint-disable-next-line
      this.state[`${field}Ref`].current.focus();
    }
  }

  handleChange(evt) {
    const target = evt.currentTarget;

    this.setState(() => ({
      [`${target.id}Value`]: target.value,
      [`${target.id}IsError`]: false,
      errorMessage: [],
      successMessage: '',
    }));
  }

  handleSubmit(evt) {
    let { nameValue, emailValue, messageValue } = this.state;

    // base values
    let focusField = false;
    let nameIsError = false;
    let emailIsError = false;
    let messageIsError = false;
    nameValue = nameValue.trim();
    emailValue = emailValue.trim();
    messageValue = messageValue.trim();

    // checks
    if (nameValue.length === 0) {
      nameIsError = true;
      focusField = focusField || 'name';
    }

    if (emailValue.length === 0 || !validateEmail(emailValue)) {
      emailIsError = true;
      focusField = focusField || 'email';
    }

    if (messageValue.length === 0) {
      messageIsError = true;
      focusField = focusField || 'message';
    }

    this.setFocus(focusField);

    this.setState({
      nameValue,
      emailValue,
      messageValue,
      nameIsError,
      emailIsError,
      messageIsError,
    }, () => {
      if (!focusField) {
        this.sendForm();
      }
    });

    evt.preventDefault();
  }

  async sendForm() {
    const { nameValue, emailValue, messageValue } = this.state;
    const data = {
      name: nameValue,
      email: emailValue,
      message: messageValue,
    };

    try {
      const res = await fetch('/api/contacts', data);

      if (res.status) {
        // form sent
        this.setState({
          successMessage: 'Thank you for your message. It was successfully sent!',
        });
      } else {
        // some errors: detect error fields & show messages
        let focusField = null;
        const newState = {
          nameIsError: false,
          emailIsError: false,
          messageIsError: false,
        };

        newState.errorMessage = res.errors.map((error) => {
          // common error
          if (typeof error === 'string') {
            return error;
          }

          // field error
          if (typeof error.message !== 'undefined') {
            focusField = focusField || error.field;
            // eslint-disable-next-line
            if (typeof error.field !== 'undefined' && typeof this.state[`${error.field}IsError`] !== 'undefined') {
              newState[`${error.field}IsError`] = true;
            }
            return error.message;
          }

          // unknown error type
          return null;
        }).filter((item) => item);

        this.setFocus(focusField);
        this.setState(newState);
      }
    } catch (err) {
      // server errors
      this.setState({
        errorMessage: 'Sorry, something happen with the server. Try again later.',
      });
    }
  }

  render() {
    const { children } = this.props;
    const params = {
      handleChange: this.handleChange,
      ...this.state,
    };

    return (
      <form onSubmit={this.handleSubmit}>
        {children(params)}
      </form>
    );
  }
}

Component.propTypes = propTypes;
Component.displayName = 'FormContacts';

export default Component;
