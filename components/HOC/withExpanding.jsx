import React from 'react';

const withExpanding = (WrappedComponent) => {
  return class extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        isExpanded: false,
      };

      this.handleClickToggle = this.handleClickToggle.bind(this);
      this.handleClickExpand = this.handleClickExpand.bind(this);
    }

    handleClickToggle(evt) {
      this.setState((state) => ({
        isExpanded: !state.isExpanded,
      }));

      evt.stopPropagation();
    }

    handleClickExpand(evt) {
      this.setState(() => ({
        isExpanded: true,
      }));

      evt.stopPropagation();
    }

    render() {
      return (
        <WrappedComponent
          onClickToggle={this.handleClickToggle}
          onClickExpand={this.handleClickExpand}
          {...this.state}
          {...this.props}
        />
      );
    }
  };
};

export default withExpanding;
