import React from 'react';
import Link from 'next/link';

const curYear = (new Date()).getFullYear();

const Component = () => (
  <div>
    <footer>
      <div className="site-width">
        <div className="menu-col">
          <h6>Links</h6>
          <nav>
            <Link href="/">
              <a href="/">Home</a>
            </Link>
            <Link href="/install-fonts">
              <a href="/install-fonts">Install fonts</a>
            </Link>
            <Link href="/about">
              <a href="/about">About</a>
            </Link>
            <Link href="/contacts">
              <a href="/contacts">Contacts</a>
            </Link>
          </nav>
        </div>

        <div className="info-col">
          <h6>About</h6>
          <div className="info">
            {'All Armenian fonts on one site!'}
            <br />
            {'Download free Armenian fonts right now.'}
          </div>
        </div>
      </div>
      <div className="site-width">
        <div className="copy">
          {`© 2015-${curYear} Armenian Fonts`}
        </div>
      </div>
    </footer>
  </div>
);

Component.displayName = 'Footer';

export default Component;
