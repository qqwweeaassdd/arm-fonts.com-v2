// @flow

import React from 'react';
import { animateScroll as scroll } from 'react-scroll';

const handleClick = (evt) => {
  evt.preventDefault();

  scroll.scrollToTop({
    duration: 500,
    smooth: true,
  });
};

class Component extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      scrollTop: 0,
    };

    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
    this.handleScroll();
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll() {
    if (typeof document !== 'undefined' && typeof window !== 'undefined') {
      const doc = document.documentElement;
      this.setState({
        scrollTop: (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0),
      });
    }
  }

  render = () => {
    const { scrollTop } = this.state;
    if (scrollTop < 565) {
      return null;
    }

    return <div title="Scroll top" className="go-top visible" onClick={handleClick} />;
  }
}

Component.displayName = 'GoTop';

export default Component;
