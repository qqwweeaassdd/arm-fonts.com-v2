// @flow

// @todo: robots.txt
// @todo: redirects to new url's
// @todo: favicon.ico

import React, { type Node } from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';

import Header from 'components/Header';
import Footer from 'components/Footer';
import SidebarFilter from 'components/sidebar/Filter';
import SidebarFontList from 'components/sidebar/FontList';
import HTMLHelperIE from 'components/html-helper/IEComment';
import GoTop from 'components/html-helper/GoTop';

const fontList = [
  'Alik U',
  'Arial AMU',
  'ArialArmST',
  'ArmBoloragir',
  'ArmErkatatarST',
  'ArmTimesST',
  'ArmTimesTytleST',
  'ArTarumianAnpuit',
  'ArTarumianBarak',
  'ArTarumianGrig',
  'ArTarumianGrqiNor',
  'ArTarumianHamagumar',
  'ArTarumianHeghnar',
  'ArTarumianIshxan',
  'ArTarumianKamar',
  'ArTarumianMatenagir',
  'ArTarumianMHarvats',
  'ArTarumianNorMatenagir',
  'ArTarumianPastar',
  'Caslon',
  'DejaVu Sans',
  'DejaVu Sans Condensed',
  'DejaVu Sans Light',
  'DejaVu Sans Mono',
  'DejaVu Serif',
  'DejaVu Serif Condensed',
  'FMBF Bardi',
  'FMBF Hanragitaran',
  'FMBF Notr',
  'FMBF Tahoma',
  'FMBF Wien',
  'FreeMono',
  'FreeSans',
  'FreeSerif',
  'HF Massis Shant N Unicode',
  'Hindsight Unicode',
  'MPH 2B Damase',
  'NorKirk',
  'Pazmaveb',
  'SenzorgaAnhok',
  'Sylfaen',
  'Times Unicode',
  'TITUS Cyberbit Basic',
  'Tribuno',
];

type Props = {
  children: Node,
  title?: string,
  pageclass?: string,
};

const propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string,
  pageclass: PropTypes.string,
};

const defaultProps = {
  title: 'arm-fonts.com',
  pageclass: '',
};

const Component = ({ children, title, pageclass }: Props) => (
  <div>
    <Head>
      <meta charSet="utf-8" />
      <meta httpEquiv="x-ua-compatible" content="ie=edge" />

      <title>{title}</title>
      <meta name="description" content="" />
      <meta name="keywords" content="" />

      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
      <link rel="icon" href="/favicon.ico" type="image/x-icon" />

      <link rel="stylesheet" href="/static/css/nprogress.css" />
      <link
        rel="stylesheet"
        href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
        integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ"
        crossOrigin="anonymous"
      />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans:400i%7CRoboto:400,700" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" />
    </Head>

    <HTMLHelperIE />

    <GoTop />

    <Header />

    <main className="site-width clear ">
      <aside>
        <SidebarFilter />
        <SidebarFontList fontList={fontList} />
      </aside>

      <section className={pageclass}>
        <h1>{title}</h1>

        {children}
      </section>
    </main>

    <Footer />
  </div>
);

Component.propTypes = propTypes;
Component.defaultProps = defaultProps;
Component.displayName = 'Layout';

export default Component;
