import React from 'react';
import Link from 'next/link';

const Component = () => (
  <div>
    <header>
      <div className="site-width">
        <div className="logo">
          <Link href="/">
            <a href="/">
              <img src="/static/img/logo.png" alt="Site logo" />
              {' Armenian Fonts'}
            </a>
          </Link>
        </div>

        <nav>
          <Link href="/">
            <a href="/">Home</a>
          </Link>
          <Link href="/best">
            <a href="/best">Best Fonts</a>
          </Link>
          <Link href="/about">
            <a href="/about">About</a>
          </Link>
          <Link href="/contacts">
            <a href="/contacts">Contacts</a>
          </Link>
        </nav>
      </div>

      <div className="top-image">
        <div className="top-image-inner">
          <p>
            {'Welcome to Armenian fonts site! Our site has one of the biggest font base with the ability of easy search of required font. You can download all Armenian fonts for free from this site including the most popular font to download '}
            <Link href="/download/Arial-AMU">
              <a href="/download/Arial-AMU">Arial Armenian</a>
            </Link>
            {' or '}
            <Link href="/?glyph=1">
              <a href="/?glyph=1">Armenian unicode fonts</a>
            </Link>
            {'. You can easily sort, filter and download Armenian fonts here. For your convenience every font style has correct preview.'}
          </p>
        </div>
      </div>
    </header>
  </div>
);

Component.displayName = 'Header';

export default Component;
