// @flow

import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import ClassNames from 'classnames';

import withExpanded from 'components/HOC/withExpanding';

type Props = {
  fontList?: Array<string>,
  isExpanded: boolean,
  onClickToggle: Function,
};

const propTypes = {
  fontList: PropTypes.arrayOf(PropTypes.string),
  isExpanded: PropTypes.bool.isRequired,
  onClickToggle: PropTypes.func.isRequired,
};

const defaultProps = {
  fontList: [],
};

const Component = ({ fontList, isExpanded, onClickToggle }: Props) => (
  <div className={ClassNames('font-list-wrapper', { visible: isExpanded })}>
    <h6>Font List</h6>
    <div className="font-list">
      <div className="font-list-gradient" onClick={onClickToggle} />

      <div>
        {fontList.map((item) => (
          <Link href={`/download?fontname=${item.replace(' ', '-')}`} as={`/download/${item.replace(' ', '-')}`} key={item}>
            <a href={`/download/${item.replace(' ', '-')}`}>{item}</a>
          </Link>
        ))}
      </div>
    </div>

    <div className="font-link font-accent" onClick={onClickToggle}>
      {isExpanded ? '▲ Collapse ▲' : '▼ Expand ▼'}
    </div>
  </div>
);

Component.displayName = 'FontList';
Component.propTypes = propTypes;
Component.defaultProps = defaultProps;

export default withExpanded(Component);
