// @flow

import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';
import Select2 from 'react-select2-wrapper';

import withExpanded from 'components/HOC/withExpanding';

type Props = {
  isExpanded: boolean,
  onClickToggle: Function,
  onClickExpand: Function,
};

const propTypes = {
  isExpanded: PropTypes.bool.isRequired,
  onClickToggle: PropTypes.func.isRequired,
  onClickExpand: PropTypes.func.isRequired,
};

const defaultProps = {
};

const Component = ({ isExpanded, onClickToggle, onClickExpand }: Props) => (
  <div className={ClassNames('font-filter', { visible: isExpanded })} onClick={onClickExpand}>
    <div className="filter-link font-accent" onClick={onClickToggle}>
      {isExpanded ? 'Collapse ▲' : 'Expand ▼'}
    </div>
    <h6>Font Filter</h6>

    <form action="/" method="post">
      <div className="form-section form-section-input">
        <button type="submit">
          <i className="fas fa-search" />
        </button>
        <input type="text" name="query" id="query" defaultValue="" placeholder="Search" />
      </div>

      <div className="hidden-fields">
        <div className="form-section">
          <h6>Type</h6>
          <div className="form-section-group clear">
            <label htmlFor="type-ttf" className="custom-control custom-control-checkbox">
              <input type="checkbox" name="type-ttf" id="type-ttf" defaultValue="1" />
              <span>
                {'TrueType '}
                <small className="font-accent">.ttf</small>
              </span>
              <span className="custom-control-indicator" />
            </label>

            <label htmlFor="type-otf" className="custom-control custom-control-checkbox">
              <input type="checkbox" name="type-otf" id="type-otf" defaultValue="1" />
              <span>
                {'OpenType '}
                <small className="font-accent">.otf</small>
              </span>
              <span className="custom-control-indicator" />
            </label>
          </div>
        </div>

        <div className="form-section">
          <h6>Weight</h6>
          <div className="form-section-group clear">
            <label htmlFor="weight-ExtraLight" className="custom-control custom-control-checkbox">
              <input type="checkbox" name="weight[]" id="weight-ExtraLight" defaultValue="1" />
              <span>
                {'ExtraLight '}
                <small className="font-accent">200</small>
              </span>
              <span className="custom-control-indicator" />
            </label>

            <label htmlFor="weight-Light" className="custom-control custom-control-checkbox">
              <input type="checkbox" name="weight[]" id="weight-Light" defaultValue="1" />
              <span>
                {'Light '}
                <small className="font-accent">300</small>
              </span>
              <span className="custom-control-indicator" />
            </label>

            <label htmlFor="weight-Regular" className="custom-control custom-control-checkbox">
              <input type="checkbox" name="weight[]" id="weight-Regular" defaultValue="1" />
              <span>
                {'Regular '}
                <small className="font-accent">400</small>
              </span>
              <span className="custom-control-indicator" />
            </label>

            <label htmlFor="weight-Medium" className="custom-control custom-control-checkbox">
              <input type="checkbox" name="weight[]" id="weight-Medium" defaultValue="1" />
              <span>
                {'Medium '}
                <small className="font-accent">500</small>
              </span>
              <span className="custom-control-indicator" />
            </label>

            <label htmlFor="weight-SemiBold" className="custom-control custom-control-checkbox">
              <input type="checkbox" name="weight[]" id="weight-SemiBold" defaultValue="1" />
              <span>
                {'SemiBold '}
                <small className="font-accent">600</small>
              </span>
              <span className="custom-control-indicator" />
            </label>

            <label htmlFor="weight-Bold" className="custom-control custom-control-checkbox">
              <input type="checkbox" name="weight[]" id="weight-Bold" defaultValue="1" />
              <span>
                {'Bold '}
                <small className="font-accent">700</small>
              </span>
              <span className="custom-control-indicator" />
            </label>

            <label htmlFor="weight-ExtraBold" className="custom-control custom-control-checkbox">
              <input type="checkbox" name="weight[]" id="weight-ExtraBold" defaultValue="1" />
              <span>
                {'ExtraBold '}
                <small className="font-accent">800</small>
              </span>
              <span className="custom-control-indicator" />
            </label>

            <label htmlFor="weight-Black" className="custom-control custom-control-checkbox">
              <input type="checkbox" name="weight[]" id="weight-Black" defaultValue="1" />
              <span>
                {'Black '}
                <small className="font-accent">900</small>
              </span>
              <span className="custom-control-indicator" />
            </label>
          </div>
        </div>

        <div className="form-section">
          <h6>Style</h6>
          <div className="form-section-group clear">
            <label htmlFor="style-italic" className="custom-control custom-control-checkbox">
              <input type="checkbox" name="style-italic" id="style-italic" defaultValue="1" />
              <span>Italic</span>
              <span className="custom-control-indicator" />
            </label>

            <label htmlFor="style-normal" className="custom-control custom-control-checkbox">
              <input type="checkbox" name="style-normal" id="style-normal" defaultValue="1" />
              <span>Normal</span>
              <span className="custom-control-indicator" />
            </label>
          </div>
        </div>

        <div className="form-section select-group">
          <h6 className="no-underline">Order</h6>
          <div className="select-wrapper">
            <Select2
              name="order"
              value="0"
              options={{ minimumResultsForSearch: -1 }}
              data={[{ text: 'By alphabet', id: 0 }, { text: 'By downloads', id: 1 }]}
            />
          </div>
        </div>

        <div className="form-section select-group">
          <h6 className="no-underline">Show</h6>
          <div className="select-wrapper">
            <Select2
              name="glyphs"
              value="1"
              options={{ minimumResultsForSearch: -1 }}
              data={[{ text: 'Unicode fonts', id: 1 }, { text: 'All fonts with Armenian glyphs', id: 0 }]}
            />
          </div>
        </div>

        <div className="form-section form-section-submit">
          <input type="submit" defaultValue="Search" />
          <a href="/" className="reset-link">Reset</a>
        </div>
      </div>
    </form>
  </div>
);

Component.displayName = 'Filter';
Component.propTypes = propTypes;
Component.defaultProps = defaultProps;

export default withExpanded(Component);
