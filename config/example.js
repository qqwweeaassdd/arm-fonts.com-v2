const path = require('path');

const root = path.join(__dirname, '..', 'server');
const staticPath = path.join(__dirname, '..', 'static-express');

module.exports = {
  root,
  staticPath,
  lib: path.join(root, '..', 'lib'),
  storage: path.join(root, '..', 'storage'),
  mailer: {
    host: 'smtp.google.com',
    port: 465,
    secure: true,
    auth: {
      user: 'mail@google.com',
      pass: '',
    },
  },
  db: {
    logging: false, // console.log,
  },
};

// require
// eslint-disable-next-line
module.exports.require = (lib) => (require(path.join(module.exports.lib, lib)));
