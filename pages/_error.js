import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';

import Layout from 'components/Layout';

import 'static/css/styles.scss';

const propTypes = {
  statusCode: PropTypes.number.isRequired,
};

const Component = ({ statusCode }) => (
  <Layout title={`${statusCode} Page`} className="site-width clear">
    <p>
      {'The requested url doesn\'t exists.'}
      <br />
      {'Please, start with the '}
      <Link href="/">
        <a href="/">main page</a>
      </Link>
      {'.'}
    </p>
  </Layout>
);

Component.getInitialProps = ({ res, err }) => {
  if (res) {
    return { statusCode: res.statusCode };
  }
  if (err) {
    return { statusCode: err.statusCode };
  }

  return { statusCode: null };
};

Component.propTypes = propTypes;

export default Component;
