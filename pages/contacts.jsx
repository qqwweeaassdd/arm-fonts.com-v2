// @flow

import React, { type createRef } from 'react';
import ClassNames from 'classnames';

import Layout from 'components/Layout';
import Input from 'components/form/Input';
import Textarea from 'components/form/Textarea';
import FormContacts from 'components/form/Contacts';

// $flow-disable-line
import 'static/css/styles.scss';

type Props = {
  nameRef: createRef<FormContacts>,
  nameValue: string,
  nameIsError: boolean,
  emailRef: createRef<FormContacts>,
  emailValue: string,
  emailIsError: boolean,
  messageRef: createRef<FormContacts>,
  messageValue: string,
  messageIsError: boolean,
  errorMessage: Array<string>,
  successMessage: string,
  handleChange: Function,
};

const Component = () => (
  <Layout title="Contacts">
    <p>
      {'If you have any information for the site administrator, please fill in this form.'}
    </p>
    <br />

    <FormContacts>
      {(props: Props) => (
        <div>
          <div className="form-section form-section-input">
            <Input
              name="contact[name]"
              id="name"
              placeholder="Your name"
              value={props.nameValue}
              ref={props.nameRef}
              className={props.nameIsError ? 'has-error' : ''}
              onChange={props.handleChange}
            />
          </div>

          <div className="form-section form-section-input">
            <Input
              type="email"
              name="contact[email]"
              id="email"
              placeholder="Email"
              value={props.emailValue}
              ref={props.emailRef}
              className={props.emailIsError ? 'has-error' : ''}
              onChange={props.handleChange}
            />
          </div>

          <div className="form-section form-section-input">
            <Textarea
              name="contact[message]"
              id="message"
              placeholder="Message"
              value={props.messageValue}
              ref={props.messageRef}
              className={props.messageIsError ? 'has-error' : ''}
              onChange={props.handleChange}
            />
          </div>

          <div className={ClassNames('text-expand text-error', { visible: props.errorMessage.length })}>
            {props.errorMessage.map((item) => (
              <div key={item}>{item}</div>
            ))}
          </div>
          <div className={ClassNames('text-expand text-success', { visible: props.successMessage })}>
            {props.successMessage}
          </div>

          <p className="form-hint font-accent">
            {'All fields are required'}
          </p>

          <div className="form-section form-section-submit">
            <input type="submit" value="Submit" />
          </div>
        </div>
      )}
    </FormContacts>
  </Layout>
);

Component.displayName = 'Contacts';

export default Component;
