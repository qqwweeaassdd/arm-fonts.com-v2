import React from 'react';

import Layout from 'components/Layout';
import FontDetail from 'components/fonts/Detail';
import DetailTopText from 'components/fonts/DetailTopText';

import 'static/css/styles.scss';
import { fetch } from '../lib/fetch';

const Stars = (props) => (
  <div {...props}>
    {[1, 2, 3, 4, 5].map((item) => <i className="fas fa-star" key={item} />)}
  </div>
);

const Component = () => (
  <Layout title="Arial AMU Armenian font" pageclass="detail-page">
    <DetailTopText />

    <div className="stat">
      <div className="font-accent">Fonts in group: 5</div>
      <div className="font-accent">Views: 19787</div>
      <div className="stars">
        <Stars className="front" style={{ width: '70%' }} />
        <Stars className="back" />
      </div>
    </div>

    <FontDetail key="1" />
    <FontDetail key="2" />

    <div className="random-fonts">
      <h6>Try other popular:</h6>
      <a href="/font-detail">download FreeSans Armenian font</a>
      <a href="/font-detail">download TITUS Cyberbit Basic Armenian font</a>
      <a href="/font-detail">download FMBF Tahoma Armenian font</a>
    </div>
  </Layout>
);

Component.getInitialProps = async function getInitialProps(context) {
  try {
    const res = await fetch('http://localhost:3000/api/data', context.query);

    // @todo: handle errors
    if (res.status) {
      console.log(res.data);
      return res.data;
    }

    // data errors
    console.log(res);
    console.error('Bad result');
  } catch (err) {
    // server errors
    console.error(err);
    console.error('Server error');
  }

  return { a: 1 };
};

Component.displayName = 'Download';

export default Component;
