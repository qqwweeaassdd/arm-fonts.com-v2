import React from 'react';

import Layout from 'components/Layout';

import 'static/css/styles.scss';

// noinspection JSUnusedGlobalSymbols
export default () => (
  <Layout title="Simple way to install fonts on Windows">
    <p>
      {'Modern versions of Microsoft Windows have many installed fonts which you can use in different editors like Microsoft Word or Adobe Photoshop. But if you work with texts a lot of time you\'ll always need something more. Are you asking yourself "How do I install a font?". Then this article for you!'}
    </p>
    <p>
      {'There is absolutely no problem to install a new font on Windows. But depending on Windows version your actions may vary.'}
    </p>
    <br />
    <br />

    <h2>
      {'How to install a font on Windows 7, Windows 8, Windows 10 and higher'}
    </h2>
    <p>
      {'Just open a directory with the font file and click right mouse button and click Install menu item:'}
    </p>
    <p>
      <img src="http://arm-fonts.com/app/pics/install-font-windows-10.jpg" alt="Install font on windows 10" />
    </p>
    <br />
    <p>
      {'That\'s all! You can install as many fonts as you wish simultaneously. Select a group of the fonts you wish to install and make the same actions like you did for one font before:'}
    </p>
    <p><img src="http://arm-fonts.com/app/pics/install-fonts-windows-10.jpg" alt="Install fonts on windows 10" /></p>
    <br />
    <br />
    <br />

    <h2>
      {'To install font on Windows XP and previous versions'}
    </h2>
    <p>
      {'Go to system fonts folder. To do it quickly press Win+R, type "fonts" and press Enter:'}
    </p>
    <p><img src="http://arm-fonts.com/app/pics/go-to-fonts-folder.jpg" alt="Go to Windows fonts folder" /></p>
    <br />
    <p>
      {'You\'ll get to system fonts folder:'}
    </p>
    <p><img src="http://arm-fonts.com/app/pics/windows-fonts-folder.jpg" alt="Windows fonts folder" /></p>
    <br />
    <p>
      {'Now copy your font file (select the font file and press Ctrl+C) and paste it to system fonts folder (set focus on Fonts folder and press Ctrl+V).'}
    </p>
    <p>
      {'Your font now installed!'}
    </p>
    <br />
    <br />
    <br />

    <p>
      {'Now there is no question "How do I install a font?" and you can use installed fonts in all windows programs including Adobe Photoshop, Microsoft Word, Microsoft Excel or any other programs supported font selection.'}
    </p>
  </Layout>
);
