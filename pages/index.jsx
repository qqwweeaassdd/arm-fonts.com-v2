import React from 'react';

import Layout from 'components/Layout';
import FontPreview from 'components/fonts/Preview';

import 'static/css/styles.scss';

// @todo: add images lazy load

// noinspection JSUnusedGlobalSymbols
export default () => (
  <Layout title="Download Armenian unicode fonts" pageclass="detail-page">
    <div className="fonts-found font-accent">Fonts found: 100</div>

    <FontPreview key="1" />
    <FontPreview key="2" />
    <FontPreview key="3" />
  </Layout>
);
