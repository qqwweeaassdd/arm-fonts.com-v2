import React from 'react';
import Link from 'next/link';

import Layout from 'components/Layout';

import 'static/css/styles.scss';

const Component = () => (
  <Layout title="About">
    <p>
      {'Hello!'}
    </p>
    <p>
      {'This small website is dedicated to fonts with Armenian letters. Fonts presented on this site are collected from all the internet. We hope that this collection will be helpful for you and simplify your search.'}
    </p>

    <p>
      {'Here you can find all kind of fonts: TrueType and OpenType Armenian fonts, bold and italic Armenian fonts, including such popular families as Arial Armenian and Times Armenian font. All fonts are grouped by font families. Flexible filter will help you to find required font shortly. Sort by downloads count feature can show you the most popular fonts immediately.'}
    </p>

    <p>
      {'All the fonts were collected from open resources and we hope usage of this fonts not trespass somebody\'s property. If you think that you have rights on any of this files, please '}
      <Link href="/contacts">
        <a href="/contacts">write to our administrator</a>
      </Link>
      {' for removing this files.'}
    </p>

    <p>
      {'Thank you for using our website!'}
    </p>
  </Layout>
);

export default Component;
