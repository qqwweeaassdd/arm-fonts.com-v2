# Nextjs boilerplate

Work with [NextJS](https://github.com/zeit/next.js/) with helpful tools:

- [SCSS](#SCSS)
- [AutoPrefixer](#AutoPrefixer)
- [CSS minification with CSSNano](#Minify-css-with-CSSNano)
- [Flow](#Flow)
- [Paths resolving](#Paths-resolving)
- [Stylelint](#Stylelint)
- [ESLint](#ESLint)
- [Debugger](#Debugger)
- [Editor Config](#Editor-Config) 

## SCSS
As `Webpack` plugin [@zeit/next-sass](https://github.com/zeit/next-plugins/tree/master/packages/next-sass)

SCSS is supposed to be a common for all project which has to be included as processed CSS-file on every page.
So you can use any markup and integrate it easily.

## AutoPrefixer
*(development only)*

As `PostCSS` plugin and [postcss-loader](https://github.com/postcss/postcss-loader) for `Webpack`. 

```
/* stylelint-disable */
// your css
/* stylelint-enable */
```
switches off checking

The tricky thing was to receive css maps after `SCSS` and `AutoPrefixer` transformations for developer
mode: `postcss-loader` don't provide sourceMap flag. That's why we use `postcssSourceMapFix` in `next.config.js`
setting this flag `true` for developer client mode, 
and [next-compose-plugins](https://www.npmjs.com/package/next-compose-plugins) to compose plugins.  

Set your own browsers to support with `./.browserslistrc` ([spec link](https://www.npmjs.com/package/browserslist)) file.

## Minify css with CSSNano
*(production  only)*

As `PostCSS` plugin [cssnano](https://github.com/cssnano/cssnano).

## Flow support
Run [Flow](https://flow.org/) with command
```
npm run flow:watch
```

`// $flow-disable-line` switches off next line checking

Flow works using [transform-flow-strip-types](https://www.npmjs.com/package/babel-plugin-transform-flow-strip-types) babel plugin.

`./flow-typed` directory contains library definitions. Other repositories for Flow library definitions can be found here:

- https://github.com/flow-typed/flow-typed (using flow-typed CLI utility) 
- https://github.com/flow-typed/flow-typed/tree/master/definitions/npm (copy directly)

#### PHPStorm configuration

Require PHPStorm setup:
Options > Languages & Frameworks > Javascript

- Switch *JavaScript Language Version* to *Flow*
- *Flow package or executable* should point to
```
./node_modules/flow-bin
```

## Paths resolving
All local import paths resolves for root directory.

How it works:

- `./.babelrc` ([spec link](https://babeljs.io/docs/en/config-files)) has [module-resolver](https://github.com/tleunen/babel-plugin-module-resolver) plugin config to resolve paths.
- `./.eslintrc` ([spec link](https://eslint.org/docs/user-guide/configuring)) has [import/resolver](https://github.com/tleunen/eslint-import-resolver-babel-module) setting config to stop highlight false errors. 
- `./.flowconfig` ([spec link](https://flow.org/en/docs/config/)) has `module.name_mapper` options that helps `flow` it to resolve root-based paths.
 
If you have other base directory, [this solution](https://github.com/tleunen/eslint-import-resolver-babel-module/issues/61#issuecomment-390283193) can be helpful.

## Stylelint (PHPStorm)
Customize [stylelint](https://github.com/stylelint/stylelint) rules in `./.stylelintrc` ([spec link](https://stylelint.io/user-guide/configuration/)).
Config extends [stylelint-config-standard](https://github.com/stylelint/stylelint-config-standard)
and work with `SCSS` using [stylelint-scss](https://www.npmjs.com/package/stylelint-scss). 

Rules:

- https://stylelint.io/user-guide/rules/ (css)
- https://www.npmjs.com/package/stylelint-scss (scss)

Require PHPStorm setup: 

Options > Languages & Frameworks > Stylesheets > Stylelint

- Enable
- Set paths to stylelint (node_modules/stylelint)

## ESlint (PHPStorm)
Customize [eslint](https://www.npmjs.com/package/eslint) rules in `./.eslintrc` ([spec link](https://eslint.org/docs/user-guide/configuring)).
Config extends [airbnb](https://github.com/airbnb/javascript) ([npm](https://www.npmjs.com/package/eslint-config-airbnb))
and work with `babel` using [babel-eslint](https://github.com/babel/babel-eslint) for Flow support. 

`// eslint-disable-next-line` switches off next line checking

Rules:

- https://eslint.org/docs/rules/ (js)
- https://github.com/yannickcr/eslint-plugin-react (react)
- https://github.com/evcohen/eslint-plugin-jsx-a11y (a11y)

## Debugging (PHPStorm)
As in [create-react-app](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md#webstorm)

## Editor Config

Set up your own personal editor config with `./.editorconfig` ([spec link](https://editorconfig.org/)) file.
