module.exports = (ctx) => {
  // noinspection SpellCheckingInspection
  const config = {
    plugins: {
      autoprefixer: {},
    },
  };

  /* if (ctx.webpack.options.hasOwnProperty('name')) {
    // ...
  } */

  // noinspection SpellCheckingInspection
  if (ctx.env === 'production') {
    config.plugins.cssnano = {};
  } else {
    // ...
  }

  return config;
};
